﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vesalica
{
    public partial class Form1 : Form
    {
        int i = 0;
        string target;
        string file;
        char slovo;
        int pokusaj;
        bool hit = false;
        

        
        public Form1()
        {
            InitializeComponent();
        }

        //biranje reci iz baze podataka
        private void nasumičnaRečToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListaReci l1 = new ListaReci();
            if (l1.ShowDialog() == DialogResult.OK)
                file = ListaReci.path;
            StreamReader sr = new StreamReader(file);
            while (sr.ReadLine() != null)
                i++;
            sr.Dispose();
            sr = new StreamReader(file);
            Random r = new Random();
            for (int k = 0; k < r.Next(1, i - 1); k++)
                target = sr.ReadLine();
            sr.Dispose();
            MessageBox.Show(target);
            Set();
        }


        //postavljanje reci i sakrivanje slova
        private void Set()
        {
            label1.Text = "";
            for (i = 0; i < target.Length; i++)
                label1.Text = label1.Text.Insert(i, "*");
        }


        private void button1_Click(object sender, EventArgs e)
        {
            
        //prazan textbox
            if (textBox1.Text == "")
            {
                MessageBox.Show("Unesi slovo, ili započni novu igru!");
                return;
            }

            slovo = textBox1.Text[0];
            
            for (i = 0; i < target.Length; i++)
            {


                if (char.IsDigit(slovo)) //provera da li je uneti karakter ispravan
                {
                    MessageBox.Show("" + slovo.ToString().ToUpper() + " nije slovo");
                    return;
                }
                

                if (Char.ToUpper(target[i]) == Char.ToUpper(slovo))
                {
                    hit = true;
                    label1.Text = label1.Text.Remove(i, 1);
                    label1.Text = label1.Text.Insert(i, slovo.ToString());
                    
                }
            }
        //pobeda
            if(label1.Text.ToUpper() == target.ToUpper())
                WonGame();

        //gubitak
            if (!hit)
            {
                
                pokusaj++;
                switch(pokusaj)
                {
                    case 1: pictureBox1.Image = Vesalica.Properties.Resources._0;
                        break;
                    case 2: pictureBox1.Image = Vesalica.Properties.Resources._1;
                        break;
                    case 3: pictureBox1.Image = Vesalica.Properties.Resources._2;
                        break;
                    case 4: pictureBox1.Image = Vesalica.Properties.Resources._3;
                        break;
                    case 5: pictureBox1.Image = Vesalica.Properties.Resources._4;
                        break;
                    case 6: pictureBox1.Image = Vesalica.Properties.Resources._5;
                        break;
                    case 7:
                        {
                            pictureBox1.Image = Vesalica.Properties.Resources._6;
                            LostGame();
                        }
                        break;
                }    
            }
            hit = false;

            textBox1.Focus();
            textBox1.SelectAll();
            

            

        }

        private void LostGame()
        {
            MessageBox.Show("Izgubio si! Reč je " +target);
            ResetGame();
        }

        private void WonGame()
        {
            MessageBox.Show("Pobeda!");
            ResetGame();
        }

        private void ResetGame()
        {
            textBox1.Text = "";
            label1.Text = "Reč";
            target = "";
            pokusaj = 0;
            pictureBox1.Image = null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ResetGame();
            MessageBox.Show("Igra je restartovana!");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        
        }
    }
}
